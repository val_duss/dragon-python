﻿#importation des differentses librairies
import pygame
from pygame.locals import *


pygame.init()
#chargement de la fenetre
fenetre = pygame.display.set_mode((640,400))
###____________________Initialisation du programme______________________###
#init couleur
GREEN = (  0, 255,   0)#couleur vert
BLUE = (0,  0, 255)#couleur bleu

#initialisation des images
perso_select = "goku"
perso_select2 = "goku_jr"
j1 = pygame.image.load("images/j1.png").convert_alpha()
j2 = pygame.image.load("images/j2.png").convert_alpha()
perso_image_select = pygame.image.load("images/goku/select.png").convert_alpha()
perso_image_select1 = pygame.image.load("images/goku_jr/select.png").convert_alpha()
perso_image_select2 = pygame.image.load("images/one_punch/select.png").convert_alpha()
bullet = pygame.image.load("images/bullet.png").convert_alpha()
bullet2 = pygame.image.load("images/bullet.png").convert_alpha()
fond = pygame.image.load("images/background.png").convert()
font = pygame.font.Font("OpenSans-Bold.ttf", 16)


fenetre.blit(fond, (0,0))

###__________Perso 1___________###

#Variable du Perso1
pv_j1 = 2000
mana_j1 = 0
dire = 1
tire = False
x_bullet = -100
y_bullet = -100
#Position
x = 100
y = 500

#Affichage perso 1
perso = pygame.image.load("images/"+perso_select+"/defaut.png").convert_alpha()
fenetre.blit(perso,( x, y))
afficher = font.render(str(pv_j1)+"/2000", 1, (255, 255, 255))
fenetre.blit(afficher, (100,100))

###__________Perso 2___________###
#Variable du Perso2
x_bullet2 = -100
y_bullet2 = -100
pv_j2 = 2000
mana_j2 = 0
dire2 = -1
tire2 = False
#Position
x2 = 400
y2 = 500

#Affichage perso 2
perso2 = pygame.image.load("images/"+perso_select2+"/defaut.png").convert_alpha()
fenetre.blit(perso2,( x2, y2))
afficher2 = font.render(str(pv_j2), 1, ( 0, 0, 0))
fenetre.blit(afficher2, (400,100))

pygame.display.flip()

#BOUCLE INFINIE

#fct pr laisser appuyer touche
pygame.key.set_repeat(50,10)

#Variables de joueurs
x_j1 = 52
x_j2 = 75
v_j1 = 1
v_j2 = 1
vald_j1 = 0
vald_j2 = 0

###____________________Boucle principale______________________###
continuer = 1

while continuer:
    #Condition de boucle
    menu = 1
    selection = 1
    jeu = 1
    #reinitialisation des variables
    x = 100
    y = 500
    pv_j1 = 2000
    mana_j1 = 0
    x2 = 400
    y2 = 500
    pv_j2 = 2000
    mana_j2 = 0
    dire = 1
    dire2 = -1
    ###____________________Boucle menu______________________###
    while menu:
        pygame.time.Clock().tick(10)
        #affichage du menu
        fenetre.blit(fond, (0,0))
        afficher3 = font.render(str("MENU"), 1, ( 0, 0, 0))
        fenetre.blit(afficher3, (250,100))
        fin8 = font.render(str("appuyez sur [espace] pour jouer"), 1, ( 0, 0, 0))
        fenetre.blit(fin8, (250,200))
        fin5 = font.render(str("appuyez sur [echap] pour quitter"), 1, ( 0, 0, 0))
        fenetre.blit(fin5, (250,250))
        #condition pour arreter ou continuer
        for event in pygame.event.get():
            pygame.event.pump()
            if event.type == QUIT:
                menu = 0
            if event.type == KEYDOWN:
                if (event.key == K_SPACE): #Si "espace"
                    menu = 0
                if (event.key == K_ESCAPE):
                    continuer = 0
                    menu = 0
                    jeu = 0
                    selection = 0
        #animation
        perso = pygame.image.load("images/"+perso_select+"/defaut.png").convert_alpha()
        #On fait monter le perso
        if y == 200:
                    if y < 800:
                        y = y - 50
                    if dire == 1:
                        perso = pygame.image.load("images/"+perso_select+"/haut.png").convert_alpha()
                    elif dire == -1:
                        perso = pygame.image.load("images/"+perso_select+"/haut_gauche.png").convert_alpha()
        #Creation de la gravité
        if y < 200:
                y = y + 5
                if dire == 1:
                    perso = pygame.image.load("images/"+perso_select+"/haut.png").convert_alpha()
                elif dire == -1:
                    perso = pygame.image.load("images/"+perso_select+"/haut_gauche.png").convert_alpha()
        else:
                y = 200
        fenetre.blit(perso, (50, y))
        pygame.display.update()
        pygame.display.flip()
    ###____________________Boucle de selection des personnages______________________###
    while selection:
        pygame.time.Clock().tick(10)
        #affichage du menu
        fenetre.blit(fond, (0,0))
        afficher6 = font.render(str("Choisissez votre personnage!"), 1, ( 0, 0, 0))
        fenetre.blit(afficher6, (250,100))
        fenetre.blit(perso_image_select, (50, 50))
        fenetre.blit(perso_image_select1, (100, 50))
        fenetre.blit(perso_image_select2, (150, 50))
        fenetre.blit(j1, (x_j1, 90))
        fenetre.blit(j2, (x_j2, 90))
        #condition pour arreter ou continuer
        for event in pygame.event.get():
            pygame.event.pump()
            if event.type == QUIT:
                menu = 0
            if event.type == KEYDOWN:
                if event.key == K_d:
                    if v_j1 == 3:
                        v_j1 = 1
                        x_j1 = 52
                    else:
                        x_j1 = x_j1 + 50
                        v_j1 = v_j1 + 1
                if event.key == K_a:
                    if v_j1 == 1:
                        v_j1 = 3
                        x_j1 = 152
                    else:
                        x_j1 = x_j1 - 50
                        v_j1 = v_j1 - 1
                if event.key == K_RIGHT:
                    if v_j2 == 3:
                        v_j2 = 1
                        x_j2 = 75
                    else:
                        x_j2 = x_j2 + 50
                        v_j2 = v_j2 + 1
                if event.key == K_LEFT:
                    if v_j2 == 1:
                        v_j2 = 3
                        x_j2 = 175
                    else:
                        x_j2 = x_j2 - 50
                        v_j2 = v_j2 - 1
                if event.key == K_x:
                    if vald_j1 != 1:
                        vald_j1 = 1
                        j1 = pygame.image.load("images/j1_confirme.png").convert_alpha()
                        if v_j1 == 1:
                            perso_select2 = "goku"
                        elif v_j1 == 2:
                            perso_select2 = "goku_jr"
                        elif v_j1 == 3:
                            perso_select2 = "one_punch"
                    else:
                        vald_j1 = 0
                        j1 = pygame.image.load("images/j1.png").convert_alpha()
                if event.key == K_KP0:
                    if vald_j2 != 1:
                        vald_j2 = 1
                        j2 = pygame.image.load("images/j2_confirme.png").convert_alpha()
                        if v_j2 == 1:
                            perso_select = "goku"
                        elif v_j2 == 2:
                            perso_select = "goku_jr"
                        elif v_j2 == 3:
                            perso_select = "one_punch"
                    else:
                        vald_j2 = 0
                        j2 = pygame.image.load("images/j2.png").convert_alpha()



                if (event.key == K_SPACE): #Si "espace"
                    if vald_j2 == 1 and vald_j1 == 1:
                        selection = 0
                    else:
                         afficher6 = font.render(str("Veuillez choisir votre personnage!!!"), 1, ( 255, 0, 0))
                         fenetre.blit(afficher6, (200,300))

                if (event.key == K_ESCAPE):
                    continuer = 0
                    menu = 0
                    selection = 0
                    jeu = 0

        #animation du perso 1

        perso = pygame.image.load("images/"+perso_select+"/defaut.png").convert_alpha()
        #On monte le perso
        if y2 == 200:
                    if y2 < 800:
                        y2 = y2 - 50
                    if dire2 == 1:
                        perso2 = pygame.image.load("images/"+perso_select2+"/haut.png").convert_alpha()
                    elif dire2 == -1:
                        perso2 = pygame.image.load("images/"+perso_select2+"/haut_gauche.png").convert_alpha()
        #Creation gravité
        if y2 < 200:
                y2 = y2 + 5
                if dire2 == 1:
                    perso2 = pygame.image.load("images/"+perso_select2+"/haut.png").convert_alpha()
                elif dire2 == -1:
                    perso2 = pygame.image.load("images/"+perso_select2+"/haut_gauche.png").convert_alpha()
        else:
                y2 = 200
        fenetre.blit(perso2, (400, y2))

        #animation du perso 2

        perso = pygame.image.load("images/"+perso_select+"/defaut.png").convert_alpha()
        #On monte le perso
        if y == 200:
                    if y < 800:
                        y = y - 50
                    if dire == 1:
                        perso = pygame.image.load("images/"+perso_select+"/haut.png").convert_alpha()
                    elif dire == -1:
                        perso = pygame.image.load("images/"+perso_select+"/haut_gauche.png").convert_alpha()
        #On creer la gravité
        if y < 200:
                y = y + 5
                if dire == 1:
                    perso = pygame.image.load("images/"+perso_select+"/haut.png").convert_alpha()
                elif dire == -1:
                    perso = pygame.image.load("images/"+perso_select+"/haut_gauche.png").convert_alpha()
        else:
                y = 200
        fenetre.blit(perso, (50, y))

        pygame.display.update()
        pygame.display.flip()

    #réinitialisation des variable
    x = 100
    y = 500
    x2 = 400
    y2 = 500
    ###____________________Boucle de JEU______________________###
    while jeu:

        pygame.time.Clock().tick(30)


        #algo direction
        if dire == 1:
            perso = pygame.image.load("images/"+perso_select+"/defaut.png").convert_alpha()
        elif dire == -1:
            perso = pygame.image.load("images/"+perso_select+"/defaut_gauche.png").convert_alpha()
        if dire2 == 1:
            perso2 = pygame.image.load("images/"+perso_select2+"/defaut.png").convert_alpha()
        elif dire2 == -1:
            perso2 = pygame.image.load("images/"+perso_select2+"/defaut_gauche.png").convert_alpha()

        for event in pygame.event.get():

            pygame.event.pump()


        #Attente des événements

            if event.type == QUIT:

                jeu = 0

            if event.type == KEYDOWN:


                #PERSO 1


                if event.key == K_UP:
                    #On monte le perso
                    if mana_j1 > 10 :#si joueur a asset de mana pour sauter
                        if y < 400: #si inferieur a 400 il perd du mana
                            y = y - 50 # diminu y
                            if y < 500:
                                mana_j1 = mana_j1 - 10 #mana diminu
                            if dire == 1:
                                perso = pygame.image.load("images/"+perso_select+"/haut.png").convert_alpha()
                            elif dire == -1:
                                perso = pygame.image.load("images/"+perso_select+"/haut_gauche.png").convert_alpha()
                    elif y == 200: # sinon il n'en perd pas
                        y = y- 50 # diminu y
                        if dire == 1:
                                perso = pygame.image.load("images/"+perso_select+"/haut.png").convert_alpha()
                        elif dire == -1:
                                perso = pygame.image.load("images/"+perso_select+"/haut_gauche.png").convert_alpha()

                if event.key == K_DOWN: #Si "flèche bas"
                    #pas d'action
                    perso = pygame.image.load("images/"+perso_select+"/bas.png").convert_alpha()
                if event.key == K_RIGHT: #Si "flèche droite"
                    #On déplace a droite le perso
                        perso = pygame.image.load("images/"+perso_select+"/droite.png").convert_alpha()
                        x = x + 20 #augmente x
                        dire = 1 #change variable de direction
                if event.key == K_LEFT: #Si "flèche gauche"

                    #On déplace a gauche le perso
                        perso = pygame.image.load("images/"+perso_select+"/gauche.png").convert_alpha()
                        x = x - 20
                        dire = -1
                if event.key == K_KP1:

                    #algo envoi de bullet
                        #par rapport a la direction
                        if dire == 1:
                            perso = pygame.image.load("images/"+perso_select+"/bullet_perso.png").convert_alpha()
                        elif dire == -1:
                            perso = pygame.image.load("images/"+perso_select+"/bullet_perso_gauche.png").convert_alpha()
                        if tire == False: #condition de variable de tire
                            if mana_j1>80: # si le personnage a asset de mana
                                mana_j1 = mana_j1 - 80 #le mana diminu
                                tire = True # alors la variable de tire en vrai
                                x_bullet = x # les coordonné de la boule de feu au départ
                                y_bullet = y # sont les même que celle du perso
                                fenetre.blit(bullet,( x_bullet, y_bullet))#affichage de la boule
                if event.key == K_KP0:
                    if x == x2: #si perso se touche
                        pv_j2 = pv_j2 - 200     #enlève des pv
                        x2 = x2 + 200*dire      #ejecte le perso
                    if dire == 1:
                        perso = pygame.image.load("images/"+perso_select+"/coup.png").convert_alpha()
                    elif dire == -1:
                        perso = pygame.image.load("images/"+perso_select+"/coup_gauche.png").convert_alpha()

                if event.key == K_KP2:
                    #recharge de mana
                       if mana_j1 < 200:    #limite
                        mana_j1 = mana_j1 + 1
                        if dire == 1:
                            perso = pygame.image.load("images/"+perso_select+"/mana.png").convert_alpha()
                        if dire == -1:
                            perso = pygame.image.load("images/"+perso_select+"/mana_gauche.png").convert_alpha()

                #PERSO 2 même condition que perso1
                if event.key == K_s:
                    perso2 = pygame.image.load("images/"+perso_select2+"/defaut.png").convert_alpha()
                if event.key == K_w:

                    if mana_j2 > 10 :
                        if y2 < 400:
                            y2 = y2 - 50
                            if y2 < 500:
                                mana_j2 = mana_j2 - 10
                            if dire2 == 1:
                                perso2 = pygame.image.load("images/"+perso_select+"/haut.png").convert_alpha()
                            elif dire2 == -1:
                                perso2 = pygame.image.load("images/"+perso_select+"/haut_gauche.png").convert_alpha()
                    elif y2 == 200:
                        y2 = y2- 50
                        if dire2 == 1:
                                perso2 = pygame.image.load("images/"+perso_select+"/haut.png").convert_alpha()
                        elif dire2 == -1:
                                perso2 = pygame.image.load("images/"+perso_select+"/haut_gauche.png").convert_alpha()
                if event.key == K_d:
                    perso2 = pygame.image.load("images/"+perso_select2+"/droite.png").convert_alpha()
                    x2 = x2 + 20
                    dire2 = 1
                if event.key == K_a:
                    perso2 = pygame.image.load("images/"+perso_select2+"/gauche.png").convert_alpha()
                    x2 = x2 - 20
                    dire2 = -1
                if event.key == K_c:
                    if dire2 == 1:
                        perso2 = pygame.image.load("images/"+perso_select2+"/bullet_perso.png").convert_alpha()
                    elif dire2 == -1:
                        perso2 = pygame.image.load("images/"+perso_select2+"/bullet_perso_gauche.png").convert_alpha()
                    if tire2 == False:
                        if mana_j2>80:
                            mana_j2 = mana_j2 - 80
                            tire2 = True
                            x_bullet2 = x2
                            y_bullet2 = y2
                            position_bullet2 = bullet2.get_rect()
                            fenetre.blit(bullet2,( x_bullet2, y_bullet2))
                if event.key == K_SPACE :
                    if x == x2:
                        pv_j1 = pv_j1 - 200
                        x = x + 200*dire2
                    if dire2 == 1:
                        perso2 = pygame.image.load("images/"+perso_select2+"/coup.png").convert_alpha()
                    elif dire2 == -1:
                        perso2 = pygame.image.load("images/"+perso_select2+"/coup_gauche.png").convert_alpha()

                if event.key == K_x:
                    if mana_j2 < 200:
                        mana_j2 = mana_j2 + 1
                        if dire2 == 1:
                            perso2 = pygame.image.load("images/"+perso_select2+"/mana.png").convert_alpha()
                        if dire2 == -1:
                            perso2 = pygame.image.load("images/"+perso_select2+"/mana_gauche.png").convert_alpha()


                if (event.key == K_ESCAPE):
                    jeu = 0

        #création de la gravité
        if y < 200:#limite du sol
                y = y + 5 # descend le personnage
                if dire == 1:
                    perso = pygame.image.load("images/"+perso_select+"/haut.png").convert_alpha()
                elif dire == -1:
                    perso = pygame.image.load("images/"+perso_select+"/haut_gauche.png").convert_alpha()
        else:
                y = 200

        if y2 < 200:
                y2 = y2 + 5
                if dire2 == 1:
                    perso2 = pygame.image.load("images/"+perso_select2+"/haut.png").convert_alpha()
                elif dire2 == -1:
                    perso2 = pygame.image.load("images/"+perso_select2+"/haut_gauche.png").convert_alpha()
        else:
                y2 = 200


        #BULLET
        if tire == True:#condition de tire
            x_bullet = x_bullet + 10*dire #direction
            if dire == 1 : #condtion de direction
                bullet = pygame.image.load("images/bullet.png").convert_alpha()
            else:
                bullet = pygame.image.load("images/bullet_gauche.png").convert_alpha()
            fenetre.blit(bullet, (x_bullet, y_bullet)) #affichage
        if x_bullet == x2 and y2 == y_bullet: #si touche autre personnage
            pv_j2 = pv_j2 - 800 #enlève pv
            tire = False    #alors pas de tire
            x_bullet = -100 #renvoi a l'exterieur de la fenètre
            y_bullet = -100
        if x_bullet < 0 or x_bullet > 640: #condition d'existence de bullet
            tire = False #pas de tire
            x_bullet = -100 #replacement de bullet a l'exterieur de la fenêtre
            y_bullet = -100
        #même condition que bullet du perso 1
        if tire2 == True:
            x_bullet2 = x_bullet2 + 10*dire2
            if dire2 == 1 : #condtion de direction
                bullet2 = pygame.image.load("images/bullet.png").convert_alpha()
            else:
                bullet2 = pygame.image.load("images/bullet_gauche.png").convert_alpha()
            fenetre.blit(bullet2, (x_bullet2, y_bullet2))
        if x_bullet2 == x and y == y_bullet2:
            pv_j1 = pv_j1 - 800
            tire2 = False
            x_bullet2 = -100
            y_bullet2 = -100
        if x_bullet2 < 0 or x_bullet2 > 640:
            tire2 = False
            x_bullet2 = -100
            y_bullet2 = -100
        #Condition d'existence des perso
        if x <= 0: #ne sort pas de la fenêtre
            x = 0
        if x >= 630:
            x = 630
        if x2 <= 0:
            x2 = 0
        if x2 >= 630:
            x2 = 630
        if y <= 0:
            y = 0
        if y2 <= 0:
            y2 = 0
        if pv_j1 < 0:#pv ne peuvent pas être négatifs
            pv_j1 = 0
        if pv_j2 < 0:
            pv_j2 = 0
        fenetre.blit(fond, (0,0))
        if pv_j1 <= 0 or pv_j2 <= 0:#condition de fin du jeu
            fin = font.render(str("GAME OVER"), 1, ( 0, 0, 0))#affichage de l'écran de fin
            fenetre.blit(fin, (250,100))
            if pv_j1 == 0:
                fin10 = font.render(str("victoire du joueur 2"), 1, ( 0, 0, 0))
                fenetre.blit(fin10, (220,175))
            elif pv_j2 == 0:
                fin10 = font.render(str("victoire du joueur 1"), 1, ( 0, 0, 0))
                fenetre.blit(fin10, (220,175))

            fin7 = font.render(str("appuyez sur [echap] pour continuer"), 1, ( 0, 0, 0))
            fenetre.blit(fin7, (170,250))
        else:
            #AFFICHAGE
            #mana
            taille_largeur_rect3 = mana_j1 / 2
            pygame.draw.rect(fenetre, BLUE, Rect((40,60), (taille_largeur_rect3,10)),0)
            taille_largeur_rect3 = mana_j2 / 2
            pygame.draw.rect(fenetre, BLUE, Rect((350,60), (taille_largeur_rect3,10)),0)
            #pv
            taille_largeur_rect1 = pv_j1 / 10
            pygame.draw.rect(fenetre, GREEN, Rect((40,40), (taille_largeur_rect1,15)),0)
            taille_largeur_rect2 = pv_j2 / 10
            pygame.draw.rect(fenetre, GREEN, Rect((350,40), (taille_largeur_rect2,15)),0)
            pygame.display.flip()

            #bullet
            fenetre.blit(bullet, (x_bullet, y_bullet))
            fenetre.blit(bullet2, (x_bullet2, y_bullet2))
            #perso
            fenetre.blit(perso, (x, y))
            fenetre.blit(perso2, (x2, y2))
            #texte
            afficher = font.render(str(pv_j1)+"/2000", 1, (255, 255, 255))
            fenetre.blit(afficher, (243,35))
            afficher2 = font.render(str(pv_j2)+"/2000", 1, ( 255, 255, 255))
            fenetre.blit(afficher2, (553,35))

        #Rafraichissement
        pygame.display.update()
        pygame.display.flip()

    #fin de la boucle
    for event in pygame.event.get():
        if event.type == KEYDOWN:

            if event.type == QUIT:
                continuer = 0

            if (event.key == K_ESCAPE):
                continuer = 0
